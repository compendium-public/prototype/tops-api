module gitlab.com/compendium-public/prototype/tops-api

go 1.14

require (
	github.com/gin-contrib/cache v1.1.0
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-contrib/gzip v0.0.3
	github.com/gin-contrib/location v0.0.2
	github.com/gin-contrib/requestid v0.0.1
	github.com/gin-gonic/gin v1.6.3
	github.com/go-git/go-git/v5 v5.2.0 // indirect
	github.com/go-redis/redis v6.15.9+incompatible // indirect
	github.com/go-redis/redis/v8 v8.4.4 // indirect
	github.com/gorilla/handlers v1.5.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/cobra v1.1.1
	github.com/spf13/viper v1.7.1
	github.com/toorop/gin-logrus v0.0.0-20210225092905-2c785434f26f
	gitlab.com/compendium-public/prototype/tops-repos v0.0.0-20210311143553-b0044bdfc364
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
