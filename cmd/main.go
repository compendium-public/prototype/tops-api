package main

import (
	cmd "gitlab.com/compendium-public/prototype/tops-api/cmd/myapp"
)

var (
	version string
)

func main() {
	cmd.Execute()
}
