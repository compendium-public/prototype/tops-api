package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var saveconfigCommand = &cobra.Command{
	Use:   "saveconfig",
	Short: "saveconfig",
	Long:  `Save the config.`,
	Args:  cobra.MinimumNArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		err := viper.WriteConfigAs(cfgFile)
		if err != nil {
			log.Println(err)
		}
	},
}

func init() {
	RootCmd.AddCommand(saveconfigCommand)
}
