package cmd

import (
	"fmt"
	"os"

	"github.com/sirupsen/logrus"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var log = logrus.New()

var cfgFile string
var Verbose bool

var (
	envPrefix = "MYAPP"
)

var RootCmd = &cobra.Command{
	Use:   "myapp",
	Short: "myapp gin server",
	Long:  `This is a sample gin app.`,
}

func Execute() {
	if err := RootCmd.Execute(); err != nil {
		log.Println(err)
		os.Exit(-1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	RootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "config.yaml", "config file (default is config.yaml)")
	RootCmd.PersistentFlags().BoolVarP(&Verbose, "verbose", "v", false, "verbose output")
	RootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func initConfig() {
	if Verbose {
		log.SetLevel(logrus.DebugLevel)
	}

	log.Debug("Loading Config")

	viper.SetEnvPrefix(envPrefix)
	viper.AutomaticEnv()

	viper.SetDefault("ADDRESS", ":5000")
	viper.SetDefault("AllowAllOrigins", true)

	viper.SetConfigName(cfgFile)
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		log.Warn(fmt.Errorf("Load config file failed: %s \n", err))
	}
}
