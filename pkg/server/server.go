package server

import (
	"os"
	"time"

	"github.com/gin-contrib/cache"
	"github.com/gin-contrib/cache/persistence"
	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/gzip"
	"github.com/gin-contrib/location"
	"github.com/gin-contrib/requestid"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	ginlogrus "github.com/toorop/gin-logrus"

	repo "gitlab.com/compendium-public/prototype/tops-repos"
	"gitlab.com/compendium-public/prototype/tops-repos/backends"
)

var (
	log         *logrus.Logger
	config      *viper.Viper
	repoServers = repo.RepoServers{}
)

func Init(c *viper.Viper, l *logrus.Logger) {
	log = l
	config = c

	repoServers.AddServer("fake", backends.NewFakeRepoBackend())
	// TODO: it better
	// but for now os.Env

	gitlabTestToken := os.Getenv("GITLAB_TOKEN_FOR_TESTING")
	gitlabEndpoint := os.Getenv("GITLAB_ENDPOINT_FOR_TESTING")
	if gitlabEndpoint == "" {
		gitlabEndpoint = "https://gitlab.dev.opsgem.cloud"
	}
	repoServers.AddServer(
		"gitlab-1",
		backends.NewGitlabRepoBackend(gitlabEndpoint, gitlabTestToken),
	)
}

func Run() {
	// Set release mode
	if config.GetBool("release") {
		log.Info("Running in release mode.")
		gin.SetMode(gin.ReleaseMode)
	}

	// Cache
	store := persistence.NewInMemoryStore(time.Second)

	// New gin router
	r := gin.New()
	r.Use(gin.Recovery())
	r.Use(location.Default())
	r.Use(requestid.New())
	r.Use(gzip.Gzip(gzip.DefaultCompression))

	// Use default logger
	if config.GetBool("use_default_logger") {
		r.Use(gin.Logger())
	} else {
		r.Use(ginlogrus.Logger(log))
	}

	// Cors Config
	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = config.GetBool("AllowAllOrigins")
	corsConfig.AllowHeaders = []string{"Origin"}
	r.Use(cors.New(corsConfig))
	// Static Files
	r.Static("/assets", "./assets")
	r.StaticFile("/favicon.ico", "./assets/favicon.ico")

	v1 := r.Group("/v1")

	// Routes
	v1.GET("/projects", CacheCheck(store), cache.CachePage(store, 120*time.Minute, projectsEndpoint))
	v1.GET("/cache_projects", cache.CachePage(store, 120*time.Minute, projectsEndpoint))

	v1.POST("/projects", projectsbyteamEndpoint)

	// Run Server
	log.Info("Listening on ", config.GetString("ADDRESS"))
	r.Run(config.GetString("ADDRESS"))
}
