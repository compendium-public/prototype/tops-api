package server

import (
	"fmt"

	"github.com/gin-gonic/gin"
	repo "gitlab.com/compendium-public/prototype/tops-repos"
)

func projectsEndpoint(c *gin.Context) {
	projects, err := repoServers.Projects()
	if err != nil {
		fmt.Println(err.Error())
	}

	projects.WithBranches()

	c.JSON(200, projects)
}

func projectsbyteamEndpoint(c *gin.Context) {
	// assume the worst
	status := 500
	var projects interface{} = "teamID required and not provided"
	var err error
	// TODO request structs... prob openapi spec
	var request map[string]interface{}
	c.BindJSON(&request)

	teamID, ok := request["teamID"]

	if ok {
		status = 200
		// grrr need a better way for this
		team := &repo.Team{FullPath: teamID.(string)}
		// run it
		projects, err = repoServers.ProjectsByTeam(team)
		if err != nil {
			status = 500
		}
	}

	c.JSON(status, projects)
}
