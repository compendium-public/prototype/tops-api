package server

import (
	"strings"

	"github.com/gin-contrib/cache/persistence"
	"github.com/gin-gonic/gin"
)

// CacheCheck sees if there are any cached responses and returns
func CacheCheck(store persistence.CacheStore) gin.HandlerFunc {
	return func(c *gin.Context) {
		// check for chache == false to remove the key and recache the next request in the store
		remove := strings.ToLower(c.Query("cache")) == "false"
		if remove {
			store.Delete(c.Request.URL.RequestURI())
		}
		c.Next()
	}
}
